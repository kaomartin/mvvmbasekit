﻿namespace MVVMBaseKit.ViewModel
{
    public interface IViewModel
    {
        void Cleanup();
        void Initialize(object payload);
    }
}
