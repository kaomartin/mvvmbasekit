﻿using System.Diagnostics.CodeAnalysis;
using GalaSoft.MvvmLight;

namespace MVVMBaseKit.ViewModel
{
    [ExcludeFromCodeCoverage]
    public abstract class ViewModel : ViewModelBase, IViewModel
    {
        public virtual void Initialize(object payload)
        {
            
        }
    }
}
