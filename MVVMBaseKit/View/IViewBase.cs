﻿namespace MVVMBaseKit.View
{
    public interface IViewBase
    {
        object DataContext { get; set; }
    }
}
