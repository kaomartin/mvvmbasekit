﻿namespace MVVMBaseKit.Message
{
    public enum NavigationAction
    {
        /// <summary>
        /// Push a view on to the view stack
        /// </summary>
        Push,

        /// <summary>
        /// pop the current view from the view stack
        /// </summary>
        Pop,

        /// <summary>
        /// Replace the current view
        /// </summary>
        Replace,

        /// <summary>
        /// Clear the stack and push the given view on to the empty stack
        /// </summary>
        Reset,

        /// <summary>
        /// Request the given view be overlayed on top of the current view of the stack.  
        /// The overlay view will not be pushed on to the stack.  
        /// If the stack is modified, the overlay view will be cleared.
        /// </summary>
        Overlay,

        /// <summary>
        /// Requests the overlay be closed
        /// </summary>
        CloseOverlay
    }
}
