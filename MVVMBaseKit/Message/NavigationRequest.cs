﻿using MVVMBaseKit.View;

namespace MVVMBaseKit.Message
{
    public class NavigationRequest
    {
        public NavigationAction NavigationAction { get; set; }
        public IViewBase View { get; set; }

        public NavigationRequest(NavigationAction navigationAction)
        {
            NavigationAction = navigationAction;
        }
    }
}
