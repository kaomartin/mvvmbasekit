﻿using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using MVVMBaseKit.Message;
using MVVMBaseKit.View;
using MVVMBaseKit.ViewModel;

namespace MVVMBaseKit.Service
{
    public class Navigator : INavigator
    {
        public void Push<TView, TViewModel>(object payload = null) 
            where TView : IViewBase
            where TViewModel : IViewModel
        {
            BuildViewAndNavigate<TView, TViewModel>(NavigationAction.Push, payload);
        }

        public void Overlay<TView, TViewModel>(object payload = null)
            where TView : IViewBase
            where TViewModel : IViewModel
        {
            BuildViewAndNavigate<TView, TViewModel>(NavigationAction.Overlay, payload);
        }

        public void Back()
        {
            SendNavigationRequest(NavigationAction.Pop);
        }

        public void CloseOverlay()
        {
           SendNavigationRequest(NavigationAction.CloseOverlay);
        }

        private void BuildViewAndNavigate<TView, TViewModel>(NavigationAction action, object payload = null)
            where TView : IViewBase
            where TViewModel : IViewModel
        {
            var navigationRequest = new NavigationRequest(action)
            {
                View = BuildView<TView, TViewModel>(payload)
            };

            SendNavigationRequest(navigationRequest);
        }

        private TView BuildView<TView, TViewModel>(object payload = null) where TView : IViewBase where TViewModel : IViewModel
        {
            var view = ServiceLocator.Current.GetInstance<TView>();
            var viewModel = ServiceLocator.Current.GetInstance<TViewModel>();

            view.DataContext = viewModel;
            viewModel.Initialize(payload);

            return view;
        }

        private void SendNavigationRequest(NavigationAction action)
        {
            SendNavigationRequest(new NavigationRequest(action));
        }

        private void SendNavigationRequest(NavigationRequest request)
        {
            Messenger.Default.Send(request);
        }
    }
}
