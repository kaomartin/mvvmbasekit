﻿using MVVMBaseKit.View;
using MVVMBaseKit.ViewModel;

namespace MVVMBaseKit.Service
{
    public interface INavigator
    {
        void Push<TView, TViewModel>(object payload = null)
            where TView : IViewBase
            where TViewModel : IViewModel;

        void Back();

        void Overlay<TView, TViewModel>(object payload = null)
            where TView : IViewBase
            where TViewModel : IViewModel;

        void CloseOverlay();
    }
}
