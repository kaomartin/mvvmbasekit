﻿using MVVMBaseKit.View;

namespace BaseKitDemo.View
{
    /// <summary>
    /// Interaction logic for PageTwoView.xaml
    /// </summary>
    public partial class PageTwoView : ViewBase, IPageTwoView
    {
        public PageTwoView()
        {
            InitializeComponent();
        }
    }
}
