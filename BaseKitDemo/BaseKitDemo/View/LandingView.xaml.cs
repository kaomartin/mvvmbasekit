﻿using MVVMBaseKit.View;

namespace BaseKitDemo.View
{
    /// <summary>
    /// Interaction logic for LandingView.xaml
    /// </summary>
    public partial class LandingView : ViewBase, ILandingView
    {
        public LandingView()
        {
            InitializeComponent();
        }
    }
}
