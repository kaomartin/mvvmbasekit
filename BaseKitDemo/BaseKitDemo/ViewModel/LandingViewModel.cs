﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseKitDemo.View;
using GalaSoft.MvvmLight.Command;
using MVVMBaseKit.Service;

namespace BaseKitDemo.ViewModel
{
    public class LandingViewModel : MVVMBaseKit.ViewModel.ViewModel, ILandingViewModel
    {
        private INavigator navigator;

        private RelayCommand navigateToPageTwoCommand = null;
        public RelayCommand NavigateToPageTwoCommand
        {
            get
            {
                return navigateToPageTwoCommand ?? (navigateToPageTwoCommand =
                           new RelayCommand(
                             () => navigator.Push<IPageTwoView, IPageTwoViewModel>()));
                
            }
        }

        public LandingViewModel(INavigator navigator)
        {
            this.navigator = navigator;
        }
    }
}
