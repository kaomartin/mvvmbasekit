﻿using System.Collections.Generic;
using BaseKitDemo.View;
using GalaSoft.MvvmLight.Messaging;
using MVVMBaseKit.Message;
using MVVMBaseKit.Service;
using MVVMBaseKit.View;
using MVVMBaseKit.ViewModel;

namespace BaseKitDemo.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// </summary>
    public class MainViewModel : MVVMBaseKit.ViewModel.ViewModel
    {
        private string _applicationTitle = "Demo Application Title";

        /// <summary>
        /// Gets the ApplicationTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ApplicationTitle
        {
            get
            {
                return _applicationTitle;
            }
            set
            {
                Set(ref _applicationTitle, value);
            }
        }

        private IViewBase currentView;
        /// <summary>
        /// The current view being displayed by the system.  It would be considered the view on top of the stack.
        /// </summary>
        public IViewBase CurrentView
        {
            get
            {
                return currentView;
            }
            set
            {
                Set(ref currentView, value);
            }

        }

        private IViewBase overlayView;
        /// <summary>
        /// The overlay view, which could be displayed over the top of or along side the CurrentView depending on the visual implementation.
        /// </summary>
        public IViewBase OverlayView
        {
            get
            {
                return overlayView;
            }
            set
            {
                Set(ref overlayView, value);
            }

        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(INavigator navigator)
        {
            NavigationStack = new Stack<IViewBase>();

            RegisterMessaging();

            navigator.Push<ILandingView, ILandingViewModel>();
        }

        private void RegisterMessaging()
        {
           Messenger.Default.Register<NavigationRequest>(this, HandleNavigation);
        }

        #region Navigation Handling

        private Stack<IViewBase> NavigationStack { get; set; }

        private void HandleNavigation(NavigationRequest navigationRequest)
        {
            switch (navigationRequest.NavigationAction)
            {
                case NavigationAction.Push:
                    NavigationStack.Push(navigationRequest.View);
                    SetCurrentViewToTopOfStack();
                    break;
                case NavigationAction.Pop:
                    DisposeCurrentView();
                    NavigationStack.Pop();
                    SetCurrentViewToTopOfStack();
                    break;
                case NavigationAction.Overlay:
                    DisposeView(OverlayView);
                    OverlayView = navigationRequest.View;
                    break;
                case NavigationAction.CloseOverlay:
                    DisposeView(OverlayView);
                    OverlayView = null;
                    break;
            }

            
        }

        private void SetCurrentViewToTopOfStack()
        {
            DisposeView(OverlayView);
            OverlayView = null;

            CurrentView = NavigationStack.Peek();
        }

        private void DisposeCurrentView()
        {
            // clean up the old view/viewModel
            DisposeView(CurrentView);
        }

        private void DisposeView(IViewBase viewToDispose)
        {
            if (viewToDispose != null)
            {
                var oldViewModel = viewToDispose.DataContext as IViewModel;
                if (oldViewModel != null)
                {
                    oldViewModel.Cleanup();
                }
                viewToDispose.DataContext = null;
            }
        }

        #endregion

       
    }
}