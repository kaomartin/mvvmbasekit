﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseKitDemo.View;
using GalaSoft.MvvmLight.Command;
using MVVMBaseKit.Service;

namespace BaseKitDemo.ViewModel
{
    public class PageTwoViewModel : MVVMBaseKit.ViewModel.ViewModel, IPageTwoViewModel
    {
        private INavigator navigator;

        private RelayCommand navigateBackCommand = null;
        public RelayCommand NavigateBackCommand
        {
            get
            {
                return navigateBackCommand ?? (navigateBackCommand =
                           new RelayCommand(
                             () => navigator.Back()));
                
            }
        }

        public PageTwoViewModel(INavigator navigator)
        {
            this.navigator = navigator;
        }
    }
}
