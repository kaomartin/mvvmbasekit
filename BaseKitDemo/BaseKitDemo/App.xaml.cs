﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using BaseKitDemo.ViewModel;
using Microsoft.Practices.ServiceLocation;

namespace BaseKitDemo
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        static App()
        {
            Bootstrapper.Startup();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var mainViewModel = ServiceLocator.Current.GetInstance<MainViewModel>();

            MainWindow window = new MainWindow();
            window.DataContext = mainViewModel;
            window.Show();
        }

    }
}
