﻿using BaseKitDemo.View;
using BaseKitDemo.ViewModel;
using GalaSoft.MvvmLight.Threading;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using MVVMBaseKit.Service;

namespace BaseKitDemo {
    public class Bootstrapper {
        public static IUnityContainer IoCContainer { get; set; }

        public static void InitilizeIoCContainer(IUnityContainer container) {
            IoCContainer = container;

            // services
            IoCContainer.RegisterType<INavigator, Navigator>();

            // views/viewModels
            IoCContainer.RegisterType<MainViewModel>();

            IoCContainer.RegisterType<ILandingViewModel, LandingViewModel>(new ContainerControlledLifetimeManager());
            IoCContainer.RegisterType<ILandingView, LandingView>();
            IoCContainer.RegisterType<IPageTwoViewModel, PageTwoViewModel>();
            IoCContainer.RegisterType<IPageTwoView, PageTwoView>();
        }

        public static void Startup() 
        {
            DispatcherHelper.Initialize();

            IUnityContainer container = new UnityContainer();

            InitilizeIoCContainer(container);

            ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(IoCContainer));
        }

    }
}
